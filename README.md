# Bionformatics. Task B: Levenshtein distance with frequencies.
This repository contains solutions for task A: Levenshtein distance between two strings with frequencies.
- Recursive solution with memory;
- Iterative solution (Dynamic programming);