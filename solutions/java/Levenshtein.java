import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.DoubleStream;


public class Levenshtein {

    public static List<Character> unique(String test){
        List<Character> temp = new ArrayList<Character>();

        for (int i = 0; i < test.length(); i++){
            char current = test.charAt(i);
            
            if (!temp.contains(current)){
               temp.add(current);
            }
        }

        Collections.sort(temp);

        return temp;
    }

    public static double find_min(double[] arr) {

        double min = arr[0];
        for(double num : arr) {
            if (num < min){
                min = num;
            }
        }
        return min;  
    }
    
    public static double sum_array(String st, int i, Map<Character, Double> map) {

        double[] arr = new double[i];
        String st2 = st.substring(0, i);

        for (int j = 0; j < st2.length(); j++) {
            arr[j] = map.get(st2.charAt(j));
        }

        double summ = DoubleStream.of(arr).sum();

        return summ;
    }
    
    public static BigDecimal lev_d(String a, String b, Map<Character, Double> map) {
        int cols = a.length() + 1;
        int rows = b.length() + 1;
        double[][] dist = new double[rows][cols];

        for (int i = 1; i < cols; i++) {
            dist[0][i] = sum_array(a, i, map);
        }

        for (int j = 1; j < rows; j++) {
            dist[j][0] = sum_array(b, j, map);
        }

        for (int i = 1; i < rows; i++) {
            for (int j = 1; j < cols; j++) {
                double[] vals = {
                    dist[i][j - 1] + map.get(a.charAt(j-1)), 
                    dist[i - 1][j] + map.get(b.charAt(i-1)),
                    dist[i - 1][j - 1] + ((a.charAt(j - 1) == b.charAt(i - 1)) ? 0 : (a.charAt(j-1) + b.charAt(i-1)))
                };

                dist[i][j] = find_min(vals);
            }
        }

        BigDecimal res = new BigDecimal(String.valueOf(dist[rows-1][cols-1])).setScale(2, RoundingMode.HALF_EVEN);
        
        return res;
    }

    public static void main(String[] args) throws IOException {

        Scanner inFile1 = new Scanner(new File("input.txt"));
        inFile1.useDelimiter(",\\s*");

        List<String> strings = new ArrayList<String>();

        while (inFile1.hasNext()) {
            strings.add(inFile1.nextLine());
        }

        String soup = String.join("", strings);
        List<Character> strings_uniq = unique(soup);

        float cnt = 0;

        Map<Character, Double> map = new HashMap<Character, Double>();
        
        for(char c : strings_uniq){
            cnt = 0;
            for(int i = 0; i < soup.length(); i++) {
                if(soup.charAt(i) == c){
                    cnt++;
                }
            }
            map.put(c, Math.round((cnt/soup.length()) * 100000.0) / 100000.0);
        }
        
        List<String> passed = new ArrayList<String>();
        BigDecimal res;
        int seq1 = 0;
        int seq2 = 0;
        BigDecimal score = new BigDecimal("9999999");
        for (int i = 0; i < strings.size(); i++) {
            for (int j = 0; j < strings.size(); j++) {
                if(strings.get(i) != strings.get(j) && !passed.contains(strings.get(j))) {
                    res = lev_d(strings.get(i), strings.get(j), map);

                    if (res.compareTo(score) == -1) {
                        seq1=i + 1; seq2=j + 1; score = res;
                    }
                    else if (res.compareTo(score) == 0 && i < seq1) {
                        seq1=i + 1; seq2=j + 1; score = res;
                    }
                    else if (res.compareTo(score) == 0 && i == seq1 && j < seq2) {
                        seq1=i + 1; seq2=j + 1; score = res;
                    }
                }
            }
            passed.add(strings.get(i));
        }

        System.out.println(seq1 + " " + seq2  + " " + score);

        FileWriter output = new FileWriter ("output.txt");
        output.write(seq1 + " " + seq2  + " " + score);
        output.close();
    }
}