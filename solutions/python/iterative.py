import time
import numpy as np

file = open('input.txt','r')
strings = file.read().splitlines()

soup = ''.join(str(i) for i in strings)
strings_uniq = sorted(set(soup))

wordfreq = [round(soup.count(w)/len(soup), 5) for w in strings_uniq]
freqs = dict(zip(strings_uniq, wordfreq))

wordfreq2 = [soup.count(w) for w in strings_uniq]
freqs2 = dict(zip(strings_uniq, wordfreq2))

def lev_d(a, b):
    cols = len(a)+1
    rows = len(b)+1
    
    dist = [[0 for x in range(cols)] for x in range(rows)]
    dist = np.zeros((rows,cols))
    
    for i in range(1, cols):
        dist[0][i] = round(sum([freqs[item] for item in a[:i]]), 5)
    
    for i in range(1, rows):
        dist[i][0] = round(sum([freqs[item] for item in b[:i]]), 5)
        
    for row in range(1, rows):
        for col in range(1, cols):
            dist[row][col] = min(round(dist[row][col-1] + freqs[a[col-1]], 5), # del
                                 round(dist[row-1][col] + freqs[b[row-1]], 5), # ins
                                 round(dist[row-1][col-1] + (0 if a[col-1] == b[row-1] else round(freqs[a[col-1]] + freqs[b[row-1]], 5)), 5)) #sub
    
    # print(dist)
    
    return round(dist[-1][-1], 5)

def find_min(strings):
    passed = []
    results = []
    for i in range(len(strings)):
        for j in range(len(strings)):
            if strings[i] != strings[j] and strings[j] not in passed:
                results.append([lev_d(strings[i], strings[j]), i + 1, j + 1])
        passed.append(strings[i])
    
    print('%d %d %.2f' % (min(results)[1], min(results)[2], min(results)[0]))
    
    f = open("output.txt","w+")
    f.write('%d %d %.2f' % (min(results)[1], min(results)[2], min(results)[0]))
    f.close()

# start_time = time.time()
find_min(strings)
# print("Took %f sec" % (time.time() - start_time))