import time
# import threading

start_time = time.time()

file = open('input.txt','r')
strings = file.read().splitlines()

soup = ''.join(str(i) for i in strings)
strings_uniq = sorted(set(soup))
wordfreq = [round(soup.count(w)/len(soup), 5) for w in strings_uniq]
freqs = dict(zip(strings_uniq, wordfreq))

lev_results = {}
def lev_d(a, b):
    if a == "":
        return round(sum([freqs[item] if item in freqs else 0 for item in b]), 5) if b != "" else 0
    if b == "":
        return round(sum([freqs[item] if item in freqs else 0 for item in a]), 5) if a != "" else 0
    
    if (a[:-1], b) not in lev_results:
        lev_results[(a[:-1], b)] = lev_d(*(a[:-1], b))
    if (a, b[:-1]) not in lev_results:
        lev_results[(a, b[:-1])] = lev_d(*(a, b[:-1]))
    if (a[:-1], b[:-1]) not in lev_results:
        lev_results[(a[:-1], b[:-1])] = lev_d(*(a[:-1], b[:-1]))


    res = min([round(lev_d(a[:-1], b) + freqs[a[-1]], 5), # del
               round(lev_d(a, b[:-1]) + freqs[b[-1]], 5), # add
               round(lev_d(a[:-1], b[:-1]) + (0 if a[-1] == b[-1] else round(freqs[a[-1]] + freqs[b[-1]], 5)), 5)]) # sub
    
    return round(res, 5)

# print(lev_d('PN', 'NP'))
# print(lev_d('NP', 'PN'))

def find_min(strings):
    passed = []
    results = []
    for i in range(len(strings)):
        for j in range(len(strings)):
            if strings[i] != strings[j] and strings[j] not in passed:
                results.append([lev_d(strings[i], strings[j]), i + 1, j + 1])
        passed.append(strings[i])
    
    f = open("output.txt","w+")
    f.write('%d %d %.2f' % (min(results)[1], min(results)[2], min(results)[0]))
    f.close()
    # print(sorted(results))
    # print('%d %d %.2f' % (min(results)[1], min(results)[2], min(results)[0]))

start_time = time.time()
find_min(strings)
print("Took %f sec" % (time.time() - start_time))


# if __name__ == '__main__':
#     jobs = []
#     for i in range(2):
#         p = multiprocessing.Process(target=find_min, args=(strings,))
#         jobs.append(p)
#         p.start()